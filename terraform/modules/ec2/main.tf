resource "tls_private_key" "infra_key" {
  algorithm = var.algorithm
}

resource "aws_key_pair" "infra-task-key" {
  key_name   = var.key_name
  public_key = tls_private_key.infra_key.public_key_openssh
}

resource "local_file" "private_key" {
  depends_on = [
    tls_private_key.infra_key,
  ]
  content  = tls_private_key.infra_key.private_key_pem
  filename = var.filename
}

resource "aws_instance" "instance" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = var.public_subnet_cidr
  vpc_security_group_ids = [var.sg_id]
  key_name               = var.key_name
  availability_zone      = var.availability_zone1
  user_data              = "${file("./userdata/setup.sh")}"

  tags = {
    Name = "instance-${var.environment}-env"
  }
}

#Outputs
output "ip_address" {
  value = aws_instance.instance.public_ip
}

output "instance_id" {
  value = aws_instance.instance.id
}

