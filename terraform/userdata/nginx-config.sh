#!/bin/bash

cd ..
sudo mv website /var/www/html
cd /var/www/html/website

cd sites-available

CONFIG_FILE="/etc/nginx/sites-available/default"

# Create or edit the default configuration file
cat << EOF | sudo tee "$CONFIG_FILE" > /dev/null
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html/website;
    index index.html index.htm;

    server_name 52.53.165.67;

    location / {
        try_files $uri $uri/ =404;
    }
}
EOF

# Verify the Nginx configuration
sudo nginx -t
sudo systemctl daemon-reload
# Restart Nginx for the changes to take effect
sudo systemctl restart nginx