<b><h1> Full CI-CD-Jenkins Project Setup on EC2</h1></b>

## Table of Contents
- [Table of Contents](#table-of-contents)
- [Pre-requisites](#pre-requisites)
- [1. Getting started with the Repository:](#1-getting-started-with-the-repository)
- [2. Provisioning the EC2 Infrastructure](#2-provisioning-the-ec2-infrastructure)
  - [Test your Installation](#test-your-installation)
- [3. Setting Up the Jenkins Server](#3-setting-up-the-jenkins-server)
- [4. Integrating Gitlab with Jenkins Server](#4-integrating-gitlab-with-jenkins-server)
  - [Installing Gitlab, Docker and Terraform Plugins](#installing-gitlab-docker-and-terraform-plugins)
  - [Create a Personal Access Token(PAT) on Gitlab](#create-a-personal-access-tokenpat-on-gitlab)
  - [Connect Gitlab's API to Jenkins with PAT](#connect-gitlabs-api-to-jenkins-with-pat)
- [5. Create and Configure Jenkins Pipeline for your project](#5-create-and-configure-jenkins-pipeline-for-your-project)
  - [Configure the Project's Repository](#configure-the-projects-repository)
  - [Add SSH Credentials for repository authentication](#add-ssh-credentials-for-repository-authentication)
  - [Adding the SSH Public key](#adding-the-ssh-public-key)
  - [Git Host Key Verification Configuration](#git-host-key-verification-configuration)
- [6. Webhook Setting for Gitlab Project](#6-webhook-setting-for-gitlab-project)
  - [Setting Webhooks for the Repository](#setting-webhooks-for-the-repository)
  - [Setting the Project's Webhook.](#setting-the-projects-webhook)
- [7. Setting Docker and AWS Credentials](#7-setting-docker-and-aws-credentials)
  - [Setting up AWS Credentials](#setting-up-aws-credentials)
  - [Setting Docker Credentials](#setting-docker-credentials)
  - [Add Jenkins to Docker group](#add-jenkins-to-docker-group)
- [8. Final Test](#8-final-test)
- [](#)

## Pre-requisites
  1. Terraform must be installed on your system.
  2. Git
  3. A Gitlab Account set up already
  4. AWS CLI

## 1. Getting started with the Repository:

  - Copy this [link](https://gitlab.com/Adephumie/my-blaze-app) to your  browser to access my repo and fork it. Then, click on the clone button to copy the url of your new repository.

    ![fork-and-clone](images/fork-clone.png "fork-and-clone")

  - On your terminal, create a new workspace or folder to start your project and pull the repo to your local machine to get started with these commands: 
  
    ```
    git init
    git pull <Forked-REPO-URL>
    git remote set-url origin <Forked-REPO-URL>

    ```

## 2. Provisioning the EC2 Infrastructure

  To provision the infrastructure for the EC2 instance:

  - Change directory to terraform folder for creating Instance.

  ```
      cd blaze-app/jenkins-infra
  ```

  - Authenticate AWS provider for Terraform commands to run. On the Terminal, type:
  
  ```
  aws configure
  ```
  - Follow the prompts to enter your credentials and configurations.

  ```
  aws configure
  AWS Access Key ID [None]: <YOUR-ACCESS-KEY-HERE>
  AWS Secret Access Key [None]: <YOUR-SECRET-KEY-HERE>
  Default region name [None]: us-west-1
  Default output format [None]: json
  ```

  - Set the provider configurations with the environment Variables you set in the last step:
  
  ```
  export AWS_ACCESS_KEY_ID="youraccesskey"
  export AWS_SECRET_ACCESS_KEY="yoursecretkey"
  export AWS_REGION="us-west-1"
  ```
  
  - Run the terraform script

  ```
  terraform init
  terraform plan
  terraform apply --auto-approve
  ```

  After this, connect to your AWS Management Console to confirm that your infrastructures have been provisioned.

  - On your terminal, still inside `jenkins-infra` directory, change the permission set for the key pair created.

  ```
   chmod 400 InfraTask.pem
  ```

  - Then, connect to the instance from your terminal with:

  ssh -i "InfraTask.pem" ubuntu@ec2-`<IP-ADDRESS-FROM-TERMINAL>`.us-west-1.compute.amazonaws.com


  NOTE THAT:-

  In the command above you will replace `<IP-ADDRESS-FROM-TERMINAL>` with the IP address that was printed on the terminal. However, you will replace the dots with dashes. For example, If the IP address you got after your terraform apply is 192.168.54.10. The command will become:

  ```
  ssh -i "InfraTask.pem" ubuntu@ec2-192-168-54-10.us-west-1.compute.amazonaws.com
  ```

  You can also get this value when you check the SSH client tab of your <b>Connect to instance</b> on the AWS Management Console.

  These series of commands will connect you to the EC2 that is already running your server.


  ### Test your Installation

  This step helps you confirm the script you ran with the userdata on ec2 instance. 

  1. To test Jenkins, type:
   
   ```
   sudo systemctl status jenkins
   ```

   You should find this:
   ![jenkins-status](images/jenkins-running.png "jenkins-status")

  2. To test Docker, type:
   
   ```
   docker version
   ```


## 3. Setting Up the Jenkins Server

   - On your web browser, go to `http://<IP-ADDRESS-FROM-TERMINAL>:8080`  to access the Jenkins Admin page.
  

  ![jenkins-administrator-page](images/jenkins-admin.png "jenkins-administrator-page")

   - On the Jenkins Administrator's page, you are to supply a secret password in the empty field. You will have to copy the file path printed in red in the image above and type the command below to your terminal:
  
  ```
  sudo cat <FILE-PATH-COPIED>
  ```

   - The command will print out the secret password on your terminal. Copy and paste it  back into the `Administrator password` field and click the `Continue` button on the screen. 

   - Then, click on the "install suggested plugins" tab.

  ![jenkins-installation](images/jenkins-installation.png "jenkins-installation")
  
   - You will automatically get to the "Create First Admin Users" page where you will fill accordingly.
  


  ![jenkins-admin](images/create-jenkin-admin.png "jenkins-admin")

  - Continue with the installation process till you get to the dashboard as shown.

  ![jenkins-dashboard](images/jenkins-dashboard.png "jenkins-dashboard")


## 4. Integrating Gitlab with Jenkins Server

  This process entails the different steps needed to set up the Jenkins server to integrate with Gitlab. 

  ### Installing Gitlab, Docker and Terraform Plugins

  - On Jenkins Dashboard, Click on <b>Manage Jenkins</b> ----> <b>System Configuration</b> ----> <b>Plugins</b> ----> <b>Available plugins</b>

    In the search field, type Gitlab and enable the following plugins: 

     - Gitlab
     - Gitlab API

  - And click on the <b>"install without restart"</b> button.
  
  - Then, search for Docker in the field and enable the following:

     - Docker
     - Docker Commons
     - Docker Pipeline
     - Docker API
     - docker-build-step
  
  - Also, click on the <b>"install without restart"</b> button.
  ![Jenkins-plugins](images/jenkins-plugins.png "Jenkins-plugins")
  
  - Repeat the same for Terraform plugin and this time, click on the <b>"Download Now and install after restart"</b> button.

  - On the next page, scroll all the way down and enable the `Restart Jenkins` part.

    ![install-plugins](images/install-plugins.png "install-plugins")

  ### Create a Personal Access Token(PAT) on Gitlab
  
  - On the topmost part of the left pane of your Gitlab Account, Click on your account to access your profile settings dropdown. Then, click on <b>Edit Profile</b> ----> <b>Access Tokens</b> 

  - Create an Access Token by specifying the name, expiration date, and enabling `api` in the scope section. Create the Token and copy it.

    ![PAT](images/PAT.png "PAT")

  ### Connect Gitlab's API to Jenkins with PAT

  - On the left pane of Jenkins dashboard, Click on <b>Manage Jenkins</b> ----> <b>System Configuration</b> ----> <b>System</b> 

  - Scroll down to <b>Gitlab</b> section 

    ![Gitlab-to-jenkins-authentication](images/Gitlab-connect-jenkin.png "Gitlab-to-jenkins-authentication")

  - For the form fields, fill in the following values:
    - Connection name = Give a descriptive name, for example, 
    - Gitlab host URL = https://gitlab.com

  - To add the credentials, Click on <b>"Add"</b> to go to the credentials creation page.

  - In the Jenkins Credentials Provider Fields:
    - Kind = GitLAb API token
    - API token = Paste your PAT generated above
    - ID = A descriptive name as shown
    - Description = Describe the key
  
  - Then, click on <b>"Add"</b>.

    ![gitlab-login-access](images/gitlab-login.png "gitlab-login-access")

  - Now click the credentials field again to add the newly created credential.
  
  - Click on <b>"Test Connection"</b> and it should return <b>Success</b> as seen in the image below.

    ![gitlab-API-connected](images/gitlab-api-connected.png "Gitlab-API-connected")

  - Finally, Click on <b>"Save"</b> at the bottom of the page to apply the configuration.


## 5. Create and Configure Jenkins Pipeline for your project
  
  On the dashboard, Click on <b>"New Item"</b>: 
  
  - On the new page, Enter a name for your project then, click on the <b>"Multibranch Pipeline"</b> and proceed.

  ### Configure the Project's Repository

  - In the Project Configuration settings page, Scroll down to <b>Branch Source</b> and in the drop-down tab, click on <b>Git</b> to expand the block.

  - For the <b>"Project Repository"</b> field, go to the repository containing your codes on Gitlab and copy the SSH URL in the <b>"Clone"</b> dropdown.

    ![ssh-url](images/ssh-url.png "ssh-url")

  - Paste into the <b>"Project Repository"</b> field on the Project's configuration page on the Jenkins server.

    NOTE:

      In this step, you can copy and paste your <b>https URL</b> to connect with the jenkins server if you used a password when setting up your gitlab username. My gitlab account was created using github for authentication, so with no password, I decided to use `ssh` authentication mode.
      
  ### Add SSH Credentials for repository authentication

  - Next is the <b>"Credentials"</b> section. Since it's a new project, you will click on <b>"Add"</b>. The dropdown will give two options, click on the name of your project. This helps us to scope the git repository's credentials to just that project's pipeline.
  
  ![multibranch-scoped-credentials](images/multi-branch-cred.png "multibranch-scoped-credentials")

  - On the new page, if you are authenticating with your gitlab's username and password, you will pick the <b>"Username With Password"</b> option in the <b>"Kind"</b> field. However, for the SSH authentication mode, you will pick the <b>"SSH Username with private key"</b> option. 

  - Fill in a descriptive name for the credential in the <b>"ID"</b> field, and add a description. In the <b>"Username"</b> section, input your Gitlab's Username.

  ![gitlab-ssh-key](images/gitlab-ssh-key.png "gitlab-ssh-key") 

  - For the <b>"Private Key"</b>, go to your local machine and from your terminal, generate an ssh key pair. I will be generating the `id_ed25519` key pair and to keep things simple, I will not add any paraphrase when asked.
  
  ```
    ssh-keygen -t ed25519
  ```

  You can also use an existing `id_rsa` key pair or generate a new one with:

  ```
  ssh-keygen
  ```
  
  However, if you already have an ssh key pair on your system, you can use them. Copy the content of the `id_ed25519` file. Note that it is the private key and not the public key with `.pub` extension.

  - Paste the private key file content into the private key field on the credentials page and save.

  - Now click on the <b>"Credentials"</b> field again to access the newly created one and add to the field.
  
  - Still on the multibranch pipeline configuration page, click on the <b>"Add"</b> button of the <b>"Behaviors"</b> section and pick the <b>"Filter by name (with regular expression)"</b> option. 

  ![discover-branch](images/discover-branch.png "discover-branch")

  To explicitly set the the specific branch we want to take effect when it pushes code to the repository, we will change the value to: `^main|stage|dev$` as shown. This will include the names of the branches we want to create. In this case, I will add two more branches to my repo namely, <b>dev</b> and <b>stage</b>.

  ![discover-specific-branch](images/regular-expression.png "discover-specific-branch")

  - In the <b>"Build Configuration"</b> field, leave the default value at `Jenkinsfile`.

  - In the <b>"Orphaned Item Strategy"</b> section, put 3 in the <b>"Max # of old Items to keep"</b> field. This will help to save space by keeping record of last three builds.

  ![maximum-build](images/max-build.png "maximum-build")

  - Scroll to the bottom of the page to save your settings.

  ### Adding the SSH Public key

  - To add an SSH key to your gitlab account, go to your gitlab Account page. On the topmost part of the left pane, Click to access your profile settings dropdown. Then, click on <b>"Edit Profile"</b> ----> <b>"SSH Keys"</b>.
  
  - Now, go to your local machine and copy the public key, the `id_ed25519.pub` key, that you generated or the existing one that you are working and paste in the SSH key field of your Gitlab's page.

  - Set the name of the key and the expiration date, then save.

  ### Git Host Key Verification Configuration

  Since we are authenticating Gitlab on Jenkins with SSH, we need to add the host to our system's <b>Known Host</b> file. So, we can achieve that by:

  ```
  ssh-keyscan -t ed25519 gitlab.com >> ~/.ssh/knownhosts
  ```

  And if you are working with an `id_rsa` key pair, use this command instead:

  ```
  ssh-keyscan -t id_rsa gitlab.com >> ~/.ssh/knownhosts
  ```


## 6. Webhook Setting for Gitlab Project

  ### Setting Webhooks for the Repository

  - On Jenkins dashboard, On the top right side of the page, locate the Account drop down tab and click on <b>"Configure"</b>. 

  ![jenkins-api-token](images/jenkins-api-token.png "jenkins-api-token")

  - On the new page, scroll to the <b>"API Token"</b> section, add a descriptive name and click on the <b>"Add new Token"</b> button.
  
  ![create-jenkins-api](images/create-jenkins-api.png "create-jenkins-api")

  
  - Copy the token generated for the next step.
 
  ### Setting the Project's Webhook.
  - Go to the repository's page on Gitlab, On the left side of the page, Click on <b>"Settings"</b>, then <b>"Webhooks"</b> and <b>"Add new webhook"</b>.
  

  ![webhook-setting](images/webhook-settings.png "webhook-setting")

  - For the <b>"URL"</b> field, fill in the address in the format below:

    http://`NAME-OF-JENKINS-ADMIN`:`JENKINS-API-TOKEN`@`IP-ADDRESS`:8080/project/`NAME-OF-MULTIBRANCH-PIPELINE`


    This means that:
      - NAME-OF-JENKINS-ADMIN: is the name you used to set up your Jenkins account above.
      - JENKINS-API-TOKEN: the API you just generated on Jenkins Account.
      - IP-ADDRESS: the public IP of your EC2 instance.
      - NAME-OF-MULTIBRANCH-PIPELINE: name of the multibranch pipeline you created on Jenkins.

  - You can enable or disable the <b>"Show full URL"</b> option, and for now, set the <b>Triggers</b> for Push events. Also, enable the <b>"Regular expression"</b> option and fill in the names of the branches we specified in the [Add SSH Credentials for repository authentication](#5-create-and-configure-jenkins-pipeline-for-your-project) section of the pipeline's configuration.

  - Scroll down to the bottom of the page, uncheck the <b>"Enable SSL verification"</b> and save. 
  
  - Click on the test button to reveal a set of actions. Click the <b>"Push Events"</b> option and look out for the notification shown in the image below. 

    ![test-webhook-connection](images/test-webhook.png "test-webhook-connection")

    Watch out for the <b>"Hook executed successfully: HTTP 200"</b> notification.
  

## 7. Setting Docker and AWS Credentials

  Because we are setting the credentials to only belong to this multibranch app, we will set our credentials under the project's scope like we did for the repository authentication.

  - On Jenkins <b>"Dashboard"</b>, Go to <b>"Manage Jenkins"</b> and then click on <b>"Credentials"</b> in the <b>"Security"</b> section. 

  - Click on the name of the multibranch project and <b>"Add New Credentials</b> under it.

  ### Setting up AWS Credentials

  - In the <b>"Kind"</b> field, Pick <b>"Secret Text"</b>.
  - Copy and Paste your AWS Access key in the <b>"Secret"</b> field.
  - Copy and Paste the ID specified in our code which is <b>"AWS_ACCESS_KEY"</b> exactly as shown below, and create credentials.

  ![AWS-credentials](images/AWS-cred.png "AWS-credentials")

  - Also, for the secret key, follow the same steps as above but copy the AWS secret key in the <b>"Secret"</b> field and type <b>"AWS_SECRET_ACCESS_KEY"</b> for the <b>"ID"</b>.


  ### Setting Docker Credentials

  The final credential to set will be the one for Docker. Click on <b>"Add New Credentials</b>. For <b>"Kind"</b>, pick <b>Username with password"</b>.

  The Username will be your docker-hub's username and put the password. For the ID- put <b>"dockerhub-credentials"</b> as it is in the Jenkinsfile.

  ![docker-credentials](images/docker-credentials.png "docker-credentials")

  ### Add Jenkins to Docker group
  
  For production implementations, it is advisable that docker should always be run by unprivileged user and should never have access to a service running in privileged mode such as jenkins directly. 
  
  So this step works for local production and testing purpose. 

  On the EC2 Terminal, type:

  ```
  sudo usermod -aG docker jenkins
  ```

  And Restart Jenkins server with:

  ```
  sudo systemctl restart jenkins
  ```

## 8. Final Test

  To test all you have set up, make a change in the repo and push to gitlab......


# Some Helpful Links

[How to pass variables into terraform modules](https://blog.geralexgr.com/terraform/pass-variables-values-inside-terraform-modules#:~:text=The%20first%20way%20you%20can,the%20definition%20of%20each%20module.)




  





















